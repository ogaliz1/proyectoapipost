<?php

// Ejemplo de uso de cabeceras en PHP

// Vamos a mostrar un PDF
header('Content-type: application/pdf');

// Se llamará downloaded.pdf
header('Content-Disposition: attachment; filename="downloaded.pdf"');

// La fuente de PDF se encuentra en original.pdf
readfile('original.pdf');
?>