<?php 

/**
 * Ejemplo de curl de la API online de Swapi
 */
$curl = curl_init('https://swapi.co/api/planets/1/');
// $curl = curl_init('data.json');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, [
  'X-RapidAPI-Host: kvstore.p.rapidapi.com',
  'X-RapidAPI-Key: 7xxxxxxxxxxxxxxxxxxxxxxx',
  'Content-Type: application/json'
]);

$response = curl_exec($curl);
curl_close($curl);

echo '<br><br>';

$datos = json_decode($response, JSON_PRETTY_PRINT);
echo var_dump($datos);


// $datos = curl_exec($curl);
// curl_close($curl);

echo '<br><br>';

echo $datos;