# URL Lanzar la lectura de los registros de la tabla Productos: 
> http://localhost:8080/proyectoapipost/api/product/read.php  

## Repasando los milestones en GitLab, gestión de proyectos  
> https://docs.gitlab.com/ee/user/project/milestones/  

### 1.0 Project Overview  
1.1 What is REST API?  
1.2 Why do we need REST API?  
1.3 Where REST API is used?  
1.4 REST API in our tutorials  
  
### 2.0 File structure  
  
### 3.0 Setup the database  
3.1 Create categories table  
3.2 Dump data for categories table  
3.3 Products table  
3.4 Dump data for products table  
3.5 Connect to database  
  
### 4.0 Read products  
4.1 Product object  
4.2 Create file to read products  
4.3 Connect to database and products table  
4.4 Read products from the database  
4.5 Add Product "read()" method  
4.6 Tell the user no products found  
4.7 Output  
  
### 5.0 Create Product  
5.1 Create create.php file  
5.2 Product create() method  
5.3 Output  
  
### 6.0 Read One Product 
6.1 Create read_one.php file  
6.2 Product readOne() method  
6.3 Output  
  
### 7.0 Update product
7.1 Create “update.php” file  
7.2 Product update() method  
7.3 Output  
  
### 8.0 Delete Product
8.1 Create “delete.php” file  
8.2 Product delete() method  
8.3 Output  
  
### 9.0 Search Products  
9.1 Create "search.php" file  
9.2 Create "search()" method  
9.3 Output  
  
### 10.0 Paginate Products
10.1 Create "read_paging.php" file  
10.2 Create "core.php" file  
10.3 Create "readPaging()" method  
10.4 Create "count()" method  
10.5 Get "paging" array  
10.6 Output  
  
### 11.0 Read Categories  
11.1 Category object  
11.2 Create "read.php" file  
11.3 Category "read()" method  
11.4 Output  
  
### 12.0 Download Source Codes  
13.0 What's Next?  
14.0 Related Tutorials  
15.0 Notes  

#### API REST CON PHP y MYSQL
* Proyecto terminado con Laragon, PHP y Mysql que nos toma dos tablas de productos y categorias (ver archivo tablas.sql 
en la raíz) que nos construye una API para lanzar datos de las tablas en JSON para ser capturados en Postman (por ejemplo).  

Para acceder al proyecto arrancamos laragon, importamos las tablas y seguiremos la documentación del siguiente enlace:  
> https://www.codeofaninja.com/2017/02/create-simple-rest-api-in-php.html   